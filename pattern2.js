// limit
function printPattern(size){
  let n = size;
  //outer loop
  for (let i = 1; i <= n; i++) {
      let str = "";
      for (let j = 1; j < i; j++) {
        str += j + " ";
      }

      for (let j = n; j >= i; j--) {
        if (i < n) {
          str += "    ";
        }
      }
      if (i === n) {
        str += n + "   ";
      }
      for (let j = i - 1; j >= 1; j--) {
        str += j + " ";
      }
    console.log(str);
  }

  // reverse outer for loop
  for (let i = n; i >= 1; i--) {
      let str = "";
      for (let j = 1; j < i; j++) {
        str += j + " ";
      }

      for (let j = n; j >= i; j--) {
        str += "    ";
      }
      for (let j = i - 1; j >= 1; j--) {
        str += j + " ";
      }
    console.log(str);
  }// end of reverse outer for loop
}// end of function

printPattern(5);